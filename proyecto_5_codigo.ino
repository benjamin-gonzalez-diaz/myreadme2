///////////////////////// Librerias y otros ////////////////////////////////////////////////////////////
#include <Stepper.h>        // importamos libreria Stepper para el motor paso a paso
#include <SoftwareSerial.h> // importamos llibreria SoftwareSerial para el modulo bluetooth
#include <EEPROM.h>          //importamos libreria EEPROM para usar el EEPROM
#include <Arduino_FreeRTOS.h> // importamos libreria RTOS
#include <LiquidCrystal.h> //importamos libreria pantalla lcd
#include <IRremote.h> // importamos libreria modulo infrarojo
#include "ListLib.h"// importamos libreria lista. sip, no pienso usar array por ahora

////////////////////////////////////////////////////////////////////////////////////////////////////////
//////  como son muchos cables, se pondra el circutio a mano ///////////////////////////////////////////
//  The circuit:
// * LCD RS pin to digital pin 19
// * LCD Enable pin to digital pin 18
// * LCD D4 pin to digital pin A3
// * LCD D5 pin to digital pin A2
// * LCD D6 pin to digital pin A1
// * LCD D7 pin to digital pin A0
// * LCD R/W pin to ground
// * LCD VSS pin to ground
// * LCD VCC pin to 5V
// * 10K resistor:
// * ends to +5V and ground
// * wiper to LCD VO pin (pin 3)
// potenciometro izq pin =  5V
// potenciometro centro pin = V0
// potenciometro der pin = GND
/////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////// variables y constantes///////////////////////////////////////////////
const int stepsPerRevolution = 200;
//void blinks(void *parametro);
const int rs = 19, en = 18, d4 = A3, d5 = A2, d6 = A1, d7 = A0;
int RECV_PIN = 7;
LiquidCrystal lcd(rs, en, d4, d5, d6, d7);
Stepper myStepper(stepsPerRevolution, 8, 10, 9, 11); // ubicacion del motor paso a paso
SoftwareSerial BTSerial ( 4 , 5 ); // ubicacion del modulo bluetooth

struct Configuracion {   // sizeof = 12
  int VeC; // velocidad de cierre
  int VeA; // velocidad de apertura
  int TiA; // Tiempo abierto
  int VeL; // velocidad del led
  int HOURS; // hora actual, Horas
  int MINUTES; // hora actual, minutos
  
};

struct listas {  // sizeof = 18
  List<int> Administrador;
  List<int> Propietario;
  List<int> Diurnos;
};
int vel = 0; // ######################### para la funcion GiroMotorPasoAPaso ######################
int dir = 0;
int check;
String c;
int val; // #######################################################################################

int vc = 30;// ######################## variable "predeterminadas" #############################
int va = 30;
float timed = 2;
int hora = 24;
int minutos = 59;
int vl = 2;// #################################################################################

String SVeC; // variables
String SVeA;
String STiA;
String SVeL;
String SH;
String SM;
String TipePassword; // si es propietario, diurno o administrador
String ModificatePassword; // si se va a crear o eliminar
String numeroPassword; // el numero de contraseña
int NUMEROPASS = 0;
int counterPorton = 0; // cantidad de veces abierto
int sizeofEEPROM;
int Password = 0; 
bool Corrida1 = false; // miles
bool Corrida2 = false; // centena
bool Corrida3 = false; // decena
bool Corrida4 = false; // unidad
int Attempts = 0; // intentos
bool cambioContrase = false;
int contar = 0;
bool BLOCK = false;
bool parpadeo = LOW;
bool Created = false; // crear contraseña
bool Eliminated = false; // eliminar contraseña
bool RESET = false;
List<int> ADMINISTRATOR; // crear lista de administrador
List<int> PROPETARY; // crear lista de propietario
List<int> DIU; // crear lista de diurno
////////////////////////////////////////////////////////////////////////////////////////////
//////////// funciones /////////////////////////////////////////////////////////////////////
void numberControl(){ // funcion con todos los botones y se va rellenando el 
    //Serial.println(IrReceiver.decodedIRData.decodedRawData);
    // IrReceiver.decodedIRData.decodedRawData --> esto es para que el modulo infrarojo me lea bien los botones y no salga el mensaje
    if(IrReceiver.decodedIRData.decodedRawData == 3125149440){
      Serial.println(1);
      PASSWORD(1);
    }
    else if(IrReceiver.decodedIRData.decodedRawData == 3108437760){
      Serial.println(2);
      PASSWORD(2);
    }
    else if(IrReceiver.decodedIRData.decodedRawData == 3091726080){
      Serial.println(3);
      PASSWORD(3);
    }
    else if(IrReceiver.decodedIRData.decodedRawData == 3141861120){
      Serial.println(4);
      PASSWORD(4);
    }
    else if(IrReceiver.decodedIRData.decodedRawData == 3208707840){
      Serial.println(5);
      PASSWORD(5);
    }
    else if(IrReceiver.decodedIRData.decodedRawData == 3158572800){
      Serial.println(6);
      PASSWORD(6);
    }
    else if(IrReceiver.decodedIRData.decodedRawData == 4161273600){
      Serial.println(7);
      PASSWORD(7);
    }
    else if(IrReceiver.decodedIRData.decodedRawData == 3927310080){
      Serial.println(8);
      PASSWORD(8);
    }
    else if(IrReceiver.decodedIRData.decodedRawData == 4127850240){
      Serial.println(9);
      PASSWORD(9);
    }
    else if(IrReceiver.decodedIRData.decodedRawData == 3860463360){
      Serial.println(0);
      PASSWORD(0);
    }
}
void reinicio(){
     Corrida1 = false;
     Corrida2 = false;
     Corrida3 = false;
     Corrida4 = false;
     Password = 0;
}
void guardoYCambio(){
  
}
void VerPassword(){ // se visualizan las listas, esto es mas porque se me cambian algunos numeros y los puedo ver el cambio
   if( cambioContrase == false){
   Serial.println("administrador:  ");
      for(int bu = 0;bu<ADMINISTRATOR.Count();bu++){
      if(ADMINISTRATOR[bu] > 10000 || ADMINISTRATOR[bu] < 0){
        ADMINISTRATOR.Remove(bu);
        ADMINISTRATOR.Add(1000);
      }
      Serial.println(ADMINISTRATOR[bu]);
    }
    Serial.println("propetario:  ");
    for(int bu = 0;bu<PROPETARY.Count();bu++){
      if(PROPETARY[bu] > 10000 || PROPETARY[bu] < 0){
        PROPETARY.Remove(bu);
        PROPETARY.Add(1000);
      }
      Serial.println(PROPETARY[bu]);
    }
    Serial.println("Diurno:  ");
    for(int bu = 0;bu<DIU.Count();bu++){
    if(DIU[bu] > 10000 || DIU[bu] < 0){
        DIU.Remove(bu);
        DIU.Add(1000);
      }
      Serial.println(DIU[bu]);
    }
    cambioContrase = true;
   }
}
void PASSWORD(int numero){ // se va rellenando el password atraves de cadenas de if, else
      if(Corrida1 == false){
        Password += numero*1000;
        Corrida1 = true;
      }
      else{
        if(Corrida2 == false){
          Password += numero*100;
          Corrida2 = true;
        }
        else{
          if(Corrida3 == false){
            Password += numero*10;
            Corrida3 = true;
          }
          else{
            if(Corrida4 == false){
              Password += numero;
              Corrida4 = true;
            }
          }
        }
      }
}
void minor10Case(int x,int pos){ // en la pantalla lcd puede pasar que no se muestre la hora 0x o minutos 0x por lo que con esto se deberia solucionar
  if(x < 10 && x > 0){
    lcd.print(0);
    lcd.setCursor(pos,1);
    lcd.print(x);
  }
  else{
    lcd.print(x); 
  }
}
void TiempoPorton(int h,int m){// se muestra el tiempo en la pantalla lcd
  lcd.setCursor(7,1);
  lcd.print("Time");
  lcd.setCursor(11,1);
  minor10Case(h,12);
  lcd.setCursor(13,1);
  lcd.print(":");
  lcd.setCursor(14,1);
  minor10Case(m,15);
}
void secuencia(int VC, int VA, float Tiempo, int Led, int horas, int minutos) { // se abre, se detiene y se cierra el "porton"
  GiroMotorPasoAPaso(VA, true, Led,counterPorton,horas,minutos); // abriendose
  lcd.setCursor(0,0);
  lcd.print("DETENIDO  ");
  delay(Tiempo * 1000);              // tiempo de espera para que se cierre
  GiroMotorPasoAPaso(VC, false, Led,counterPorton,horas,minutos); // cerrandose
  lcd.setCursor(0,0);
  lcd.print("DETENIDO  ");
  counterPorton+=1;
}
void GiroMotorPasoAPaso(int tiempo, bool opcion, int led, int counterGate, int h, int m) { // se mueve el motor
  int x = 0;
  while (x <= 80) {
    c = "200,C";
    char str_array[c.length() + 1];
    c.toCharArray(str_array, c.length() + 1);
    str_array[c.length()] = ' ';
    check = 0;
    char * token = strtok(str_array, ",");
    if(opcion == true){
      lcd.setCursor(0,0);
      lcd.print("ABRIENDO  ");
    }
    else if(opcion == false){
      lcd.setCursor(0,0);
      lcd.print("CERRANDO  ");
    }
    lcd.setCursor(0,1);
    lcd.print("Count");
    lcd.setCursor(5,1);
    lcd.print(counterGate);
    TiempoPorton(h,m);
    while ( token != NULL ) {
      vel = tiempo;
      if (opcion == true) {
        dir = 1;
      }
      else {
        dir = -1;
      }
      token = strtok(NULL, ",");
    }
    if (digitalRead(12) == HIGH && opcion == false) {
      BTSerial.println("_Stop");
      break;
    }
    if (digitalRead(6) == HIGH && opcion == true) {
      BTSerial.println("Stop_");
      break;
    }
    if ((millis() / 1000) % led == 0) {
      digitalWrite(3, LOW);
    }
    else {
      digitalWrite(3, HIGH);
    }
    myStepper.setSpeed(vel);
    myStepper.step(stepsPerRevolution * dir);
    x++;
  }
}
//////////////////////////////////////////////////////////////////////////
///////////////////////// Codigo /////////////////////////////////////////
void setup() {
  /////////////////////////// EEPROM ////////////////////////////////////
  Configuracion ExtractEEPROM;
  sizeofEEPROM = 12 + 1;
  EEPROM.get(sizeofEEPROM, ExtractEEPROM);
  vc = ExtractEEPROM.VeC;
  va = ExtractEEPROM.VeA;
  timed = ExtractEEPROM.TiA;
  vl = ExtractEEPROM.VeL;
  hora = ExtractEEPROM.HOURS;
  minutos = ExtractEEPROM.MINUTES;
  
  listas ListasEEPROM;  
  EEPROM.get(53,ListasEEPROM);
  ADMINISTRATOR = ListasEEPROM.Administrador;
  PROPETARY = ListasEEPROM.Propietario;
  DIU = ListasEEPROM.Diurnos;
  ////////////////////////////////////////////////////////////////////
//  xTaskCreate(blinks,"blink",128,NULL,3,NULL); // prioridad alta
  Serial.begin(9600);
  BTSerial.begin(9600);
  lcd.begin(16, 2);
  IrReceiver.begin(RECV_PIN, ENABLE_LED_FEEDBACK);
  pinMode(3, OUTPUT);
  pinMode(12, OUTPUT);
  pinMode(2, INPUT);
  pinMode(6, OUTPUT);
  digitalWrite(3,LOW);
/////////////////////////////// lo que viene es por siacaso se bugea el EEPROM ////////////////////////
//  ADMINISTRATOR.Add(1234);
//  PROPETARY.Add(2345);
//  DIU.Add(1467);
/////////////////////////////////////////////////////////////////////////////////////////////////////
}

void loop() {
  /////////////////////////////////////////////////////// modulo bluetooth ///////////////////////////////////////////////
  VerPassword();
  if (BTSerial.available()) { // MODULO BLUETOOTH
    SVeC = BTSerial.readStringUntil(','); 
    if(SVeC == "si"){
      ModificatePassword = BTSerial.readStringUntil(',');
      Serial.println(ModificatePassword);
      TipePassword = BTSerial.readStringUntil(',');
      numeroPassword = BTSerial.readStringUntil(',');
      NUMEROPASS = numeroPassword.toInt(); 
      if(ModificatePassword == "c"){
        if(TipePassword == "p"){
          Serial.println("llego aqui");
          PROPETARY.Add(NUMEROPASS);
        }
        else if(TipePassword == "d" ){
          DIU.Add(NUMEROPASS);
        }
        else if(TipePassword == "a"){
          ADMINISTRATOR.Add(NUMEROPASS);
        }
      }
         if(ModificatePassword == "e"){
          if(TipePassword == "p"){
          Serial.println("Finalmente");
            PROPETARY.Remove(PROPETARY.IndexOf(NUMEROPASS));
          }
          else if(TipePassword == "d" ){
            Serial.println("Finalmente");
            DIU.Remove(DIU.IndexOf(NUMEROPASS));
          }
        else if(TipePassword == "a"){
          ADMINISTRATOR.Remove(ADMINISTRATOR.IndexOf(NUMEROPASS));
        }
      }
      listas ListasEEP;
      ListasEEP.Administrador = ADMINISTRATOR;
      ListasEEP.Propietario = PROPETARY;
      ListasEEP.Diurnos = DIU;
      EEPROM.put(53,ListasEEP);
    }
    else{
          vc = SVeC.toInt();                    // dato velocidad de cerrado 
    SVeA = BTSerial.readStringUntil(','); 
    va = SVeA.toInt();                    // dato velocidad de apertura
    STiA = BTSerial.readStringUntil(','); 
    timed = STiA.toInt();                 // tiempo parpadeo
    SVeL = BTSerial.readStringUntil(','); 
    vl = SVeL.toInt();                    // tiempo detenido
    SH = BTSerial.readStringUntil(':'); 
    hora = SH.toInt();                    // hora actual, hora
    SM = BTSerial.readStringUntil('\n'); 
    minutos = SM.toInt();                 // hora actual, minuto
    if (vl < 2) {
      vl = 2;
      BTSerial.write("Velocidad DE parpadeo demasiado chica, se cambiara a 2\n");
    }
    else if (vl > 10) {
      vl = 10;
      BTSerial.write("velocidad DE parpadeo demasiado Grande, se cambiara a 10\n");
    }
    if(hora > 24){
      hora = 24;
    }
    else if(hora < 0){
      hora = 0;
    }
    if(minutos > 59){
      minutos = 59;
    }
    else if(minutos < 0){
      minutos = 0;
    }
    BTSerial.write("Se ha Guardado\n");
    sizeofEEPROM = sizeof(Configuracion) + 1;
    Configuracion SaveEEPROM;
    SaveEEPROM.VeC = vc;
    SaveEEPROM.VeA = va;
    SaveEEPROM.TiA = timed;
    SaveEEPROM.VeL = vl;
    SaveEEPROM.HOURS = hora;
    SaveEEPROM.MINUTES = minutos;
    EEPROM.put(sizeofEEPROM, SaveEEPROM);
    }
  }
  //////////////////////////////////////////////////////////////////////////////////////////////////////////
  digitalWrite(3, LOW);
  if (digitalRead(2) == HIGH) { // BOTON
    Serial.println("_______________________");
    Serial.print("Velocidad de cerrado: ");
    Serial.println(vc);
    Serial.print("Velocidad de apertura: ");
    Serial.println(va);
    Serial.print("Tiempo porton abierto: ");
    Serial.println(timed);
    Serial.print("Velocidad parpadeo led: ");
    Serial.println(vl);
    Serial.print("tiempo: ");
    Serial.print(hora);
    Serial.print(":");
    Serial.println(minutos);
    Serial.println("_______________________");
    secuencia(vc, va, timed, vl,hora,minutos);
  }
  if (IrReceiver.decode()) { // MODULO INFRAROJO
    //Serial.println(IrReceiver.decodedIRData.decodedRawData);
    numberControl();
    if(IrReceiver.decodedIRData.decodedRawData == 3910598400){// *
      Serial.println("*");
      cambioContrase = false;
      VerPassword();
    }
    else if(IrReceiver.decodedIRData.decodedRawData == 4144561920){ // <|, crear
      Serial.println("<|");
      if(ADMINISTRATOR.Contains(Password) == 1){
        Created = true;
      }
      reinicio();
    }
    else if(IrReceiver.decodedIRData.decodedRawData == 2774204160){// |>, eliminar
      Serial.println("|>");
      if(ADMINISTRATOR.Contains(Password) == 1){
        Eliminated = true;
      }
      reinicio();
    }
    else if(IrReceiver.decodedIRData.decodedRawData == 2907897600){ // v crer o eliminar propietario
      Serial.println("v");
      if(Created){
        PROPETARY.Add(Password); 
        Created = false;
        Eliminated = false;
      }
      if(Eliminated){
        PROPETARY.Remove(PROPETARY.IndexOf(Password));
        Created = false;
        Eliminated = false;
      }
      reinicio();
      listas ListasEEP;
      ListasEEP.Administrador = ADMINISTRATOR;
      ListasEEP.Propietario = PROPETARY;
      ListasEEP.Diurnos = DIU;
      EEPROM.put(53,ListasEEP);
      wdt_reset();
    }
    else if(IrReceiver.decodedIRData.decodedRawData == 3877175040){ // A, crear o eliminar diurno
      Serial.println("A");
      if(Created){
        DIU.Add(Password);
        Created = false;
        Eliminated = false;
      }
      if(Eliminated){
        DIU.Remove(DIU.IndexOf(Password));
        Created = false;
        Eliminated = false;
      }
      reinicio();
      listas ListasEEP;
      ListasEEP.Administrador = ADMINISTRATOR;
      ListasEEP.Propietario = PROPETARY;
      ListasEEP.Diurnos = DIU;
      EEPROM.put(53,ListasEEP);
      wdt_reset();
    }
    else if(IrReceiver.decodedIRData.decodedRawData == 4061003520){ // #,crear o eliminar administrador
      Serial.println("#");
      if(Created){
        ADMINISTRATOR.Add(Password);
        Created = false;
        Eliminated = false;
      }
      if(Eliminated){
        ADMINISTRATOR.Remove(DIU.IndexOf(Password));
        Created = false;
        Eliminated = false;
      }
      reinicio();
      listas ListasEEP;
      ListasEEP.Administrador = ADMINISTRATOR;
      ListasEEP.Propietario = PROPETARY;
      ListasEEP.Diurnos = DIU;
      EEPROM.put(53,ListasEEP);
      wdt_reset();
    }
    else if(IrReceiver.decodedIRData.decodedRawData == 3810328320){ // ok
      if(BLOCK == false && Created == false && Eliminated == false){
          Serial.println(Password);
          Serial.print("intentos: ");
          Serial.println(2-Attempts);
          if(DIU.Contains(Password) == 1 && hora > 6 && hora < 19){
            lcd.setCursor(0,0);
            secuencia(vc, va, timed, vl,hora,minutos);
            Attempts = 0;
          }
          else if(PROPETARY.Contains(Password) == 1){
            lcd.setCursor(0,0);
            secuencia(vc, va, timed, vl,hora,minutos);
            Attempts = 0;
          }
          else{
            lcd.setCursor(0,0);
            lcd.print("INCORRECTO");
            Attempts ++;
          }      
          if(Attempts >= 3){
            lcd.setCursor(0,0);
            lcd.print("BLOQUEADO   ");
            Attempts = 0;
            BLOCK = true;
          }
         Corrida1 = false;
         Corrida2 = false;
         Corrida3 = false;
         Corrida4 = false;
         Password = 0;
        }
        else{
         Created = false; // se restaura el created
         Eliminated = false;// se restaura el eliminated
        }
    }
    IrReceiver.resume();
  }
  if(BLOCK){
    if((millis())%100==0){
        digitalWrite(3,parpadeo);
        delay(100);
        parpadeo = !parpadeo;
        digitalWrite(3,parpadeo);
        delay(100);
      contar++;
    }
    if(contar==25){
      BLOCK = false;
      contar = 0;
    }
  }
}
